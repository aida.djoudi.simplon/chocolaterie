<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>La chocolaterie</title>

        <!--Global CSS--> 
        <link rel="stylesheet" href="../style/global.css">
        
    </head>
    <body>
        <?php include "layout/header.php"; ?>

            <main>

                <section>

                    <article>
                        
                        <h2 class="titlecontenttem">Soyez le bienvenue dans la chocolaterie de la planète Simplon</h2>
                
                        <img class="imgindex" src="../medias/photo.jpg" alt="Photo chocolaterie Simplon" title="Photo chocolaterie Simplon"/>

                    </article>
            
                </section>
                
            </main>
            
        <?php include "layout/footer.php"; ?>

    </body>
</html>