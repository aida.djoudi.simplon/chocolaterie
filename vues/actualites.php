<!DOCTYPE html>
<html lang="fr">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title>La chocolaterie</title>

        <!--Global CSS--> 
        <link rel="stylesheet" href="../style/global.css">
        
    </head>
    <body>
        <?php include "layout/header.php"; ?>

            <main>

                <section class="bodyactus">

                    <h2>Actus du moment:</h2>

                    <?php
                        require "../db/config.php";
                        require "../db/connectDB.php";

                        $connect = new ConnectDB($config);
                        $actualites = $connect->showactus();
                    ?>

                    <?php foreach($actualites as $actualite): ?>
                        <article class="actus">
                            <h3 class="titleactus"><?= $actualite["title"] ?></h3>
                            <p class="contentactus"><?= $actualite["content"] ?></p>
                        </article>
                    <?php endforeach; ?> 

                </section>
                
            </main>

        <?php include "layout/footer.php"; ?>

        <script src="../js/actus.js"></script>

    </body>
</html>