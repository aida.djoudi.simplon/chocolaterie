
<header class="title">
    <h1 class="animated infinite bounce delay-10s" prefix="La "> Chocolaterie</h1>
</header>
<nav>
    <ul>
        <li><a href="/vues/index.php">Accueil</a></li>
        <li><a href="/vues/actualites.php">Actualités</a></li>
        <li><a href="/vues/produits.php">Produits</a></li>
        <li><a href="/vues/equipe.php">Equipe</a></li>
        <li><a href="/vues/contact.php">Contact</a></li>
    </ul>
</nav>
