const products = [
]

const productsWrapperElem=document.getElementById("shopProducts");

// récupère les valeurs des produits depuis le DOM
// (dont les données sont récupérées depuis la db)
const shopElems = productsWrapperElem.getElementsByClassName("shop-element")
Array.from(shopElems).forEach(shopElem => {
    const product = {
        title: shopElem.dataset.name,
        desc: shopElem.dataset.description,
        picPath: shopElem.dataset.image,
        picAlt:"",
    };

    products.push(product);
});


var productIndex = 0;
const carouselImgElem = document.getElementById("carouselImg");
const carouselTitleElem = document.getElementById("caourselTitle");
const caourselDescElem = document.getElementById("caourselDesc");

function switchCarouselProduct() {
    if (productIndex >= products.length) {
        productIndex = 0;
    }

    carouselImgElem.src = products[productIndex].picPath;
    carouselImgElem.alt = products[productIndex].picAlt;
    carouselTitleElem.innerText = products[productIndex].title;
    caourselDescElem.innerText = products[productIndex].desc;

    productIndex++;
}

switchCarouselProduct();
setInterval(switchCarouselProduct, 5 * 1000);