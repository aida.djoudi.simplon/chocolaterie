entreprise
===
la chocolaterie simplon

secteur d'activité
===
vitrine de magasin, puis vente en ligne plus tard

design
===
inspiré de https://www.chocolateriedesbauges.com/index.html

façon de travailler
===
- groupe sur gitlab
- dépôt pour le groupe
- tout le monde à les permissions Owner
- une personne par page
- une branche par page
- on commit le plus possible après chaque modification fonctionelle
- les conflits sont géré entre ceux qui ont écrit le code en conflit
- on utilise la branche dev-php pour migrer les pages du html au php et utiliser la db
- on utilise PHP pour intégrer les header/footer commun à toutes les pages
- quand la transition sera finie, on mergera tous dans master (puis on mergera master dans nos branches respectives)
- fichiers html/php/etc déstinés à être visible sur le site dans le dossier vues/, segments commun (header/footer) dans vues/layout
- fichiers CSS et dérivés dans le dossier style/
- fichiers images/vidéos dans le dossier medias/
- javascript dans js/
- tout ce qui touche à la db (sauf les vues) dans db/

installation
===
```
apt install php
apt install php-sqlite3

cd /path/to/project/chocolaterie

sh db/reset.sh

php --server localhost:8090
xdg-open http://localhost:8090/vues
#ACHETEZ DU CHOCOLAT!!!!
```
