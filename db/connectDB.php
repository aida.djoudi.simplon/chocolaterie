<?php 

class ConnectDB 
{
    private $_config=[];
    private $_bdd;

    public function __construct(array $config)
    {

        //Tableau des paramètres de configuration connexion BDD
        $this->_config = $config;

    	try {

            //Connexion à la base de données MySQL
            // $this->_bdd = new PDO($this->_config["host_mysql"],
            //                       $this->_config["username_mysql"],
            //                       $this->_config["pwd_mysql"],
            //                       array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES \'UTF8\'',
            //                             PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            //                             PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true)); 


            //Connexion à la base de données SQLite
            $this->_bdd = new PDO($this->_config["host_sqlite"]);
            $this->_bdd->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
            $this->_bdd->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    		
    	}catch(PDOException $ex){
            
            die("Erreur lors de la connexion à la base de données !!!!!" . $ex->getMessage());
            
    	}
    }

    public function connect()
    {
        return $this->_bdd;
    }

    public function showactus():array
    {   
        $actualites=[];
        $conn = $this->connect();
        $sql = "SELECT * FROM actualites ORDER BY id DESC";
        $actus = $conn->query($sql);
        foreach ($actus as $actu) {
            $actualites[] = $actu;
        }
        return $actualites;
        
    }

    public function getProducts():array
    {
        $products=[];
        $conn = $this->connect();
        $sql = "SELECT * FROM products";
        $results = $conn->query($sql);
        foreach ($results as $result) {
            $products[] = $result;
        }
        return $products;
    }

}

